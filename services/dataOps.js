const dataFile = require('../data')

const { data } = dataFile

const filteredData = (pattern) => {
    return data.reduce((acc, curr) => {
        const { people } = curr;
        const currData = {
            name: curr.name,
            people: []
        }
        people.forEach(person => {
            const { animals } = person;
            const currPerson = {
                name: person.name,
                animals: []
            }
            animals.forEach(animal => {
                const { name } = animal
                if (name.includes(pattern)) currPerson.animals.push(animal)
            })
            if (currPerson.animals.length) currData.people.push(currPerson)
        })
        return currData.people.length ? [...acc, currData] : acc;
    }, [])
}

const count = data.reduce((acc, curr) => {
    const { people } = curr;
    const currData = {
        ...curr,
        name: `${curr.name} [${curr.people.length}]`,
        people: []
    }
    people.forEach(person => {
        const { animals } = person;
        const currPerson = {
            name: `${person.name} [${person.animals.length}]`,
            animals: [...person.animals]
        }
        currData.people.push(currPerson)
    })
    return [...acc, currData];

}, [])

module.exports = {
    filteredData: filteredData,
    count: count,
}