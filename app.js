const dataOps = require('./services/dataOps')

const cliArg = process.argv[2].split('--')[1].split('=')
if (cliArg[0] === 'count') {
    console.log(JSON.stringify(dataOps.count,null,1))
}
else if (cliArg[0] === 'filter') {
    const filterResult = dataOps.filteredData(cliArg[1])
    if (filterResult.length == 0) {
        console.log('NO DATA FOUND')
    }
    else {
        console.log(JSON.stringify(filterResult, null, 1))
    }
}
else {
    console.log('Command not found')
}
